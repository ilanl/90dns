Open a console on your OpenWrt router and enter the following to add the 
required configuration entries:
```
uci batch <<'EOF'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.com/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.net/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.jp/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.co.uk/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo-europe.com/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendowifi.net/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/conntest.nintendowifi.net/95.216.149.205'
add_list dhcp.@dnsmasq[-1].address='/ctest.cdn.nintendo.net/95.216.149.205'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.es/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.co.kr/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.tw/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.com.hk/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.com.au/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.co.nz/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.at/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.be/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendods.cz/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.dk/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.de/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.fi/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.fr/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.gr/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.hu/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.it/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.nl/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.no/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.pt/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.ru/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.co.za/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.se/0.0.0.0'
add_list dhcp.@dnsmasq[-1].address='/.nintendo.ch/0.0.0.0'
commit dhcp
EOF
```

This will apply the new configuration and reload dnsmasq:
```
/etc/init.d/dnsmasq reload
```

You can check for success for example with the Nslookup Diagnostics tool in
LUCI.
